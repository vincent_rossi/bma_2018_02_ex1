package exercice_2018_01;

import static org.junit.Assert.assertArrayEquals;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

import org.junit.experimental.theories.DataPoints;
import org.junit.experimental.theories.FromDataPoints;
import org.junit.experimental.theories.Theories;
import org.junit.experimental.theories.Theory;
import org.junit.runner.RunWith;

@RunWith(Theories.class)
public class AppTest {

	public static @DataPoints("files") String[] paths = { "inputs/signal_S.csv", "inputs/signal_M.csv",
			"inputs/signal_L.csv", "inputs/signal_XL.csv" };
	// public static @DataPoints("files") String[] paths = { "inputs/signal_S.csv",
	// "inputs/signal_M.csv" };

	@Theory
	public void test_construction_of_matrix_from_file(@FromDataPoints("files") String path) throws IOException {
		AppInterface appRef = new App();
		AppInterface appToTest = new AppCandidat();

		double[][] fromFileToMatrix = appRef.fromFileToMatrix(path);
		double[][] fromFileToMatrix2 = appToTest.fromFileToMatrix(path);

		assertArrayEquals(fromFileToMatrix, fromFileToMatrix2);
	}

	@Theory
	public void test_rounding_matrix(@FromDataPoints("files") String path) throws IOException {
		AppInterface appRef = new App();
		AppInterface appToTest = new AppCandidat();

		double[][] fromFileToMatrix = appRef.fromFileToMatrix(path);
		double[][] fromFileToMatrix2 = appToTest.fromFileToMatrix(path);

		int[][] roundedMatrix = appRef.getRoundedMatrix(fromFileToMatrix);
		int[][] roundedMatrix2 = appToTest.getRoundedMatrix(fromFileToMatrix2);

		assertArrayEquals(roundedMatrix, roundedMatrix2);
	}

	@Theory
	public void test_rounding_matrix_via_file(@FromDataPoints("files") String path) throws IOException {
		AppInterface appRef = new App();
		AppInterface appToTest = new AppCandidat();

		double[][] fromFileToMatrix = appRef.fromFileToMatrix(path);
		double[][] fromFileToMatrix2 = appToTest.fromFileToMatrix(path);

		int[][] roundedMatrix = appRef.getRoundedMatrix(fromFileToMatrix);
		int[][] roundedMatrix2 = appToTest.getRoundedMatrix(fromFileToMatrix2);

		Utils.fromMatrixToFile(roundedMatrix, "1.csv");
		Utils.fromMatrixToFile(roundedMatrix2, "2.csv");

		byte[] bytes1 = Files.readAllBytes(new File("1.csv").toPath());
		byte[] bytes2 = Files.readAllBytes(new File("2.csv").toPath());

		assertArrayEquals(bytes1, bytes2);
	}

	@Theory
	public void test_averaging_matrix(@FromDataPoints("files") String path) throws IOException {
		AppInterface appRef = new App();
		AppInterface appToTest = new AppCandidat();

		double[][] fromFileToMatrix = appRef.fromFileToMatrix(path);
		double[][] fromFileToMatrix2 = appToTest.fromFileToMatrix(path);

		int[][] roundedMatrix = appRef.getRoundedMatrix(fromFileToMatrix);
		double[][] averagedCompressedMatrix = appRef.getAveragedCompressedMatrix(roundedMatrix);

		int[][] roundedMatrix2 = appToTest.getRoundedMatrix(fromFileToMatrix2);
		double[][] averagedCompressedMatrix2 = appToTest.getAveragedCompressedMatrix(roundedMatrix2);

		assertArrayEquals(averagedCompressedMatrix, averagedCompressedMatrix2);
	}

	@Theory
	public void test_averaging_matrix_via_file(@FromDataPoints("files") String path) throws IOException {
		AppInterface appRef = new App();
		AppInterface appToTest = new AppCandidat();

		double[][] fromFileToMatrix = appRef.fromFileToMatrix(path);
		double[][] fromFileToMatrix2 = appToTest.fromFileToMatrix(path);

		double[][] averagedCompressedMatrix = appRef
				.getAveragedCompressedMatrix(appRef.getRoundedMatrix(fromFileToMatrix));

		double[][] averagedCompressedMatrix2 = appToTest
				.getAveragedCompressedMatrix(appToTest.getRoundedMatrix(fromFileToMatrix2));

		Utils.fromMatrixToFile(averagedCompressedMatrix, "1.csv");
		Utils.fromMatrixToFile(averagedCompressedMatrix2, "2.csv");

		byte[] bytes1 = Files.readAllBytes(new File("1.csv").toPath());
		byte[] bytes2 = Files.readAllBytes(new File("2.csv").toPath());

		assertArrayEquals(bytes1, bytes2);
	}
}
