package exercice_2018_01;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.util.List;

public class App2 implements AppInterface {

	public static void main(String[] args) throws IOException {
	}

	public void computeResults() throws IOException {
		String[] values = new String[] { "S", "M", "L", "XL" };
		for (String i : values) {
			double[][] inputMatrix = fromFileToMatrix("inputs/signal_" + i + ".csv");
			int[][] roundedMatrix = getRoundedMatrix(inputMatrix);
			double[][] avgCompressedMatrix = getAveragedCompressedMatrix(roundedMatrix);
			Utils.fromMatrixToFile(roundedMatrix, "inputs/signal_" + i + "_round.csv");
			Utils.fromMatrixToFile(avgCompressedMatrix, "inputs/signal_" + i + "_compressed.csv");
		}
	}

	@Override
	public double[][] getAveragedCompressedMatrix(int[][] inputMatrix) {
		if (Utils.testBothDimensionsDividableByTwo(inputMatrix)) {
			double[][] outputMatrix = new double[inputMatrix.length / 2][inputMatrix[0].length / 2];
			for (int i = 0; i < inputMatrix.length; i += 2) {
				int[] line1 = inputMatrix[i];
				int[] line2 = inputMatrix[i + 1];
				for (int j = 0; j < line1.length; j += 2) {
					int top_left = line1[j];
					int top_right = line1[j + 1];
					int bottom_left = line2[j];
					int bottom_right = line2[j + 1];
					double two = 2d;
					outputMatrix[i / 2][j / 2] = (((top_left + top_right) / two) + ((bottom_left + bottom_right) / two))
							/ two;
				}
			}
			return outputMatrix;
		} else {
			return null;
		}
	}

	@Override
	public double[][] fromFileToMatrix(String path) throws IOException {
		File inputFile = new File(path);
		List<String> readAllLines = Files.readAllLines(inputFile.toPath(), Charset.forName("UTF-8"));
		String firstLine = readAllLines.get(0);
		int width = firstLine.split("[,]").length;
		int height = readAllLines.size();
		double[][] inputMatrix = new double[height][width];
		int row = 0;
		int col = 0;
		for (String line : readAllLines) {
			String[] fields = line.split("[,]");
			for (String string : fields) {
				inputMatrix[row][col] = Double.parseDouble(string);
				col++;
			}
			row++;
			col = 0;
		}
		return inputMatrix;
	}

	@Override
	public int[][] getRoundedMatrix(double[][] inputMatrix) {
		int[][] roundedMatrix = new int[inputMatrix.length][inputMatrix[0].length];
		for (int i = 0; i < inputMatrix.length; i++) {
			double[] ds = inputMatrix[i];
			for (int j = 0; j < ds.length; j++) {
				double d = ds[j];
				BigDecimal bigDecimal = new BigDecimal(d);
				roundedMatrix[i][j] = bigDecimal.setScale(0, RoundingMode.HALF_UP).intValue();
			}
		}
		return roundedMatrix;
	}
}
