package exercice_2018_01;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.math.BigDecimal;

public class Utils {

	/**
	 * Write the content of <code>inputMatrix</code> to a file referenced by
	 * <code>path</code>
	 * 
	 * @param inputMatrix
	 * @param path
	 * @throws IOException
	 */
	public static void fromMatrixToFile(int[][] inputMatrix, String path) throws IOException {
		BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(path));
		for (int i = 0; i < inputMatrix.length; i++) {
			StringBuilder stringBuilder = new StringBuilder();
			int[] ds = inputMatrix[i];
			stringBuilder.append(ds[0]);
			for (int j = 1; j < ds.length; j++) {
				int d = ds[j];
				stringBuilder.append(",").append(d);
			}
			bufferedWriter.write(stringBuilder.toString());
			bufferedWriter.newLine();
		}
		bufferedWriter.close();
	}

	/**
	 * Write the content of <code>inputMatrix</code> to a file referenced by
	 * <code>path</code>
	 * 
	 * @param inputMatrix
	 * @param path
	 * @throws IOException
	 */
	public static void fromMatrixToFile(double[][] inputMatrix, String path) throws IOException {
		BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(path));
		for (int i = 0; i < inputMatrix.length; i++) {
			StringBuilder stringBuilder = new StringBuilder();
			double[] ds = inputMatrix[i];
			stringBuilder.append(ds[0]);
			for (int j = 1; j < ds.length; j++) {
				double d = ds[j];
				stringBuilder.append(",").append(d);
			}
			bufferedWriter.write(stringBuilder.toString());
			bufferedWriter.newLine();
		}
		bufferedWriter.close();
	}

	/**
	 * !!! THIS METHOD IS USED IN TESTS, DO NOT ALTER !!!
	 * 
	 * @param inputMatrix
	 * @return a copy of inputMatrix with BigDecimal reduced to the double primitive
	 *         type
	 */
	public static double[][] convertFromBigDecimalToDouble(BigDecimal[][] inputMatrix) {
		double[][] result = new double[inputMatrix[0].length][inputMatrix.length];
		for (int i = 0; i < inputMatrix.length; i++) {
			for (int j = 0; j < inputMatrix[0].length; j++) {
				result[i][j] = inputMatrix[j][i].doubleValue();
			}
		}
		return result;
	}

	/**
	 * Can be used to print to the console the look of the matrix Will use
	 * {@link #toString()} method so careful about the Objects going in
	 * 
	 * @param inputMatrix
	 */
	public static void printMatrix(Object[][] inputMatrix) {
		for (int i = 0; i < inputMatrix.length; i++) {
			Object[] ds = inputMatrix[i];
			System.out.print(ds[0]);
			for (int j = 1; j < ds.length; j++) {
				System.out.print(" | " + ds[j]);
			}
			System.out.println();
		}
	}

	/**
	 * Same as {@link #printMatrix(Object[][])} but for double primitive type
	 * 
	 * @param inputMatrix
	 */
	public static void printMatrix(double[][] inputMatrix) {
		for (int i = 0; i < inputMatrix.length; i++) {
			double[] ds = inputMatrix[i];
			System.out.print(ds[0]);
			for (int j = 1; j < ds.length; j++) {
				System.out.print(" | " + ds[j]);
			}
			System.out.println();
		}
	}

	/**
	 * Same as {@link #printMatrix(Object[][])} but for int primitive type
	 * 
	 * @param inputMatrix
	 */
	public static void printMatrix(int[][] inputMatrix) {
		for (int i = 0; i < inputMatrix.length; i++) {
			int[] ds = inputMatrix[i];
			System.out.print(ds[0]);
			for (int j = 1; j < ds.length; j++) {
				System.out.print(" | " + ds[j]);
			}
			System.out.println();
		}
	}

	/**
	 * Test if the given matrix has both dimensions being multiples of 2 (or not)
	 * 
	 * @param matrix
	 * @return true if matrix dimensions are both dividable by 2 (so can be shrunk)
	 */
	public static boolean testBothDimensionsDividableByTwo(double[][] matrix) {
		if (matrix.length % 2 == 1 || matrix[0].length % 2 == 1) {
			return false;
		}
		return true;
	}

	/**
	 * Test if the given matrix has both dimensions being multiples of 2 (or not)
	 * 
	 * @param matrix
	 * @return true if matrix dimensions are both dividable by 2 (so can be shrunk)
	 */
	public static boolean testBothDimensionsDividableByTwo(int[][] matrix) {
		if (matrix.length % 2 == 1 || matrix[0].length % 2 == 1) {
			return false;
		}
		return true;
	}

}
