package exercice_2018_01;

import java.io.IOException;
import java.util.Random;

public class Init {

	public static final int[] values = new int[] { 4, 10, 100, 1000 };

	public static void main(String[] args) {
		Init.generateMatrices();
	}

	public static void generateMatrices() {
		for (int i : values) {
			double[][] generateMatrix = generateMatrix(i, i);
			try {
				Utils.fromMatrixToFile(generateMatrix, i + "x" + i + ".csv");
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	public static double[][] generateMatrix(int width, int height) {
		Random random = new Random();
		double[][] matrix = new double[height][width];
		for (int row = 0; row < height; row++) {
			for (int col = 0; col < width; col++) {
				// Reducing to 4 decimal points
				matrix[row][col] = Double.parseDouble(String.valueOf(random.nextDouble()).substring(0, 5));
			}
		}
		return matrix;
	}

}
