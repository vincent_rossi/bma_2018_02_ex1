# Exercice 1: Voiture autonome Signal
Dans cet exercice, vous allez développer la partie signal d'une voiture autonome.

##Sensing : Lecture
Vous devez ici lire, à partir d'un fichier, les informations d'un capteur. Lire un fichier CSV et retourner un tableau (matrice) de la taille du fichier et de type double[nb lignes][nb colonnes]

Ajoutez votre code dans la méthode fromFileToMatrix de l'exercice 1. Ne touchez pas aux paramètres.

fromFileToMatrix(path) output: tableau de la 'taille' du fichier (lignes, colonnes)

Pour cette partie, nous vous encourageons à utiliser java.nio.file.Files (plain java) et String.split

Vous avez également accès (si vous le souhaitez) à :
- org.apache.commons.commons-lang3 (3.6)
- com.google.guava.guava (23.0)
- org.apache.commons.commons-io (1.3.2)

##Sensing : signal
La partie sensor de la voiture est chargée de capter des signaux, les nettoyer et les envoyer à la partie suivante. Cette fonction recevra un array de doubles de 0.0 à 1.0. Vous devrez retourner un array d'integer de 0 à 1 en arrondissant à l'unité la plus proche. 0,5 => 1.Le tableau retourné doit être de la taille de celui en entrée.

##Sensing : compression
A partir d'une image nettoyée, compressez l'image en réalisant la moyenne des 4 cases adjacentes. Le tableau retourné sera de dimensions deux fois inférieures au tableau entré.

Exemple : [[0, 0, 1, 1], [1, 0, 1, 1], [0, 0, 1, 0], [1, 1, 1, 0]]

retournera: [[0.25, 1.0], [0.5, 0.5]]

Le 0.25 (1ère ligne, 1ère colonne) est ainsi la moyenne des positions [0][0], [0][1], [1][0], [1][1].

##Fonctionnement en Java

Vous ne devez modifier que le fichier AppCandidat.java

Utilisez les commentaires inline de AppCandidat.java pour vous aiguiller